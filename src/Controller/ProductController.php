<?php

namespace App\Controller;

use App\Entity\Content;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use App\Entity\ProductType;


class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index(): Response
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();
        $contents = $this->getDoctrine()->getRepository(Content::class)->findAll();

        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'products' => $products,
            'types' => $types,
            'contents' => $contents
        ]);
    }

    /**
     * @Route("/produits/categorie/{category}", name="productByCategory")
     */
    public function productByCategory(string $category): Response
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findByCategory($category);
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();
        $contents = $this->getDoctrine()->getRepository(Content::class)->findAll();

        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'products' => $products,
            'types' => $types,
            'contents' => $contents
        ]);
    }

    /**
     * @Route("/produits/type/{type}", name="productByType")
     */
    public function productByType(string $type): Response
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findByProductType($type);
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();
        $contents = $this->getDoctrine()->getRepository(Content::class)->findAll();

        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'products' => $products,
            'types' => $types,
            'contents' => $contents
        ]);
    }

    /**
     * @Route("/products/{id}", name="product")
     */
    public function productDetails($id): Response
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();
        $contents = $this->getDoctrine()->getRepository(Content::class)->findAll();

        return $this->render('product/details.html.twig', [
            'controller_name' => 'ProductController',
            'product' => $product,
            'types' => $types,
            'contents' => $contents
        ]);
    }
}
