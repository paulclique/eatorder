<?php

namespace App\Controller;

use App\Entity\Content;
use App\Entity\ProductType;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class PanierController extends AbstractController
{
    /**
     * @Route("/panier", name="panier")
     */
    public function index(SessionInterface $session, ProductRepository $productRepository): Response
    {
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();
        $panier = $session->get('panier', []);
        $contents = $this->getDoctrine()->getRepository(Content::class)->findAll();

        $panierWithData = [];

        foreach($panier as $id => $quantity) {
            $panierWithData[] = [
                'product' => $productRepository->find($id),
                'quantity' => $quantity,
                'contents' => $contents
            ];
        }

        // dd($panierWithData);

        $total = 0;

        foreach($panierWithData as $item) {
            $totalItem = $item['product']->getPrice() * $item['quantity'];
            $total += $totalItem;
        }

        return $this->render('panier/index.html.twig', [
            'controller_name' => 'PanierController',
            'types' => $types,
            'items' => $panierWithData,
            'total' => $total,
            'contents' => $contents
        ]);
    }

    /**
     * @Route("/panier/add/{id}", name="panier_add")
     */
    public function add($id, SessionInterface $session)
    {    
       $panier = $session->get('panier', []);
       $contents = $this->getDoctrine()->getRepository(Content::class)->findAll();

       if (!empty($panier[$id])) {
            $panier[$id]++;
       } else {
            $panier[$id] = 1;
       }

       $session->set('panier', $panier);

    //    dd($session->get('panier'));

       return $this->redirectToRoute("panier");
    }

    /**
     * @Route("/panier/remove/{id}", name="panier_remove")
     */
    public function remove($id, SessionInterface $session)
    {    
       $panier = $session->get('panier', []);

       if (!empty($panier[$id])) {
            unset($panier[$id]);
       }

       $session->set('panier', $panier);

       return $this->redirectToRoute("panier");
    }
}
