<?php

namespace App\Controller;

use App\Entity\Content;
use App\Entity\DrinkInfo;
use App\Entity\FoodInfo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ProductType;
use App\Entity\Product;
use App\Entity\TypeCategory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AdministrationController extends AbstractController
{

    // INDEX //

    /**
     * @Route("/administration", name="administration")
     */
    public function index(): Response
    {
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();

        return $this->render('administration/index.html.twig', [
            'controller_name' => 'AdministrationController',
            'types' => $types,
            'products' => $products
        ]);
    }

    // end : INDEX //

    // LIST OF PRODUCTS //

    /**
     * @Route("/administration/produits", name="administration")
     */
    public function products(): Response
    {
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();

        return $this->render('administration/product/products.html.twig', [
            'controller_name' => 'AdministrationController',
            'types' => $types,
            'products' => $products
        ]);
    }

    // end : LIST OF PRODUCTS //

    // ADD PRODUCT //

    /**
     * @Route("/administration/product/add", name="add_product")
     */
    public function addProduct(EntityManagerInterface $entityManager, Request $request): Response
    {
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();

        $product = new Product();

        // Création du formulaire
        $form = $this->createFormBuilder($product)
            ->add('name', TextType::class, array('required' => true))
            ->add('description', TextType::class, array('required' => true))
            ->add('image', FileType::class, array('required' => true))
            ->add('price', IntegerType::class, array('required' => true))
            ->add(
                'productType',
                EntityType::class,
                [
                    'class' => ProductType::class,
                    'choice_label' => "name",
                    'mapped' => false
                ],
                array('required' => true)
            )
            ->add(
                'category',
                EntityType::class,
                [
                    'class' => TypeCategory::class,
                    'choice_label' => "name",
                    'mapped' => false
                ],
                array('required' => true)
            )
            ->add('save', SubmitType::class, [
                'label' => 'Add',
                'attr' => ['class' => 'btn-full']

            ])
            ->getForm();

        //Récupération des données
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $product->setProductType($form->get("productType")->getData());
            $product->setCategory($form->get("category")->getData());

            $file = $form->get('image')->getData();
            //Récupération des données
            $product = $form->getData();
            $product->setImage($file->getClientOriginalName());
            //Sauvegardes des données
            $entityManager->persist($product);
            //Execution de l'insertion des données
            $entityManager->flush();

            // Sauvegarde de l'image
            $file->move('assets/img/products', $file->getClientOriginalName());

            // //Sauvegarde du fichier
            // $file->move('assets/img/products/' . $product->getProductType()->getName() . '/', $file->getClientOriginalName());

            // Redirection 
            return $this->redirectToRoute('adminProducts');

        }

        return $this->render('administration/product/add.html.twig', [
            'controller_name' => 'AdministrationController',
            'form' => $form->createView(),
            'action' => 'Add new product',
            'types' => $types
        ]);

    }

    // end : ADD PRODUCT //

    // ADD PRODUCT INFORMATIONS //

    /**
     * @Route("/administration/produits/update/{id}/informations/add", name="add_infos_product")
     */
    public function addInfoProduct(int $id, EntityManagerInterface $entityManager, Request $request): Response
    {

        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();

        if ($product->getProductType()->getName() == "Plats" || $product->getProductType()->getName() == "Snacks") {
            $foodinfo = new FoodInfo();
            $form = $this->createFormBuilder($foodinfo)
                ->add('fat', IntegerType::class, array('required' => false))
                ->add('energy', IntegerType::class, array('required' => false))
                ->add('glucid', IntegerType::class, array('required' => false))
                ->add('protein', IntegerType::class, array('required' => false))
                ->add('sugar', IntegerType::class, array('required' => false))
                ->add('salt', IntegerType::class, array('required' => false))

                ->add('save', SubmitType::class, ['label' => "Ajouter les food infos"])
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                //Sauvegarde des données
                $entityManager->persist($foodinfo);

                $foodinfo->setFat($form->get("fat")->getData());
                $foodinfo->setEnergy($form->get("energy")->getData());
                $foodinfo->setGlucid($form->get("glucid")->getData());
                $foodinfo->setProtein($form->get("protein")->getData());
                $foodinfo->setSugar($form->get("sugar")->getData());
                $foodinfo->setSalt($form->get("salt")->getData());

                $product->setFoodInfo($foodinfo);

                //Exectution de la requête
                $entityManager->flush();

                //Redirection
                return $this->redirectToRoute('product', ['id' => $id]);

            }
        }

        if ($product->getProductType()->getName() == "Boissons") {
            $drinkinfo = new DrinkInfo();
            $form = $this->createFormBuilder($drinkinfo)
                ->add('liter', IntegerType::class, array('required' => false))
                ->add('alcohol', IntegerType::class, array('required' => false))

                ->add('save', SubmitType::class, ['label' => "Update les drink infos"])
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                //Sauvegarde des données
                $entityManager->persist($drinkinfo);

                $drinkinfo->setLiter($form->get("liter")->getData());
                $drinkinfo->setAlcohol($form->get("alcohol")->getData());

                $product->setDrinkInfo($drinkinfo);

                //Exectution de la requête
                $entityManager->flush();

                //Redirection
                return $this->redirectToRoute('adminProducts', ['id' => $id]);

            }
        }

        return $this->render('administration/product/add-informations.html.twig', [
            'controller_name' => 'AdminController',
            'form' => $form->createView(),
            "types" => $types,
            'action' => 'Ajouter des infos à un produit',
        ]);
    }

    // end : ADD PRODUCT INFORMATIONS //

    // UPDATE PRODUCT //

    /**
     * @Route("/administration/produits/update/{id}", name="update_product")
     */
    public function updateProduct(int $id, EntityManagerInterface $entityManager, Request $request): Response
    {
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        //Récupération du manager de données
        $entityManager = $this->getDoctrine()->getManager();

        //Sélection d'une donnée par son id
        $product = $entityManager->getRepository(Product::class)->find($id);

        //Vérification si le produit existe bien
        if (!$product) {
            throw $this->createNotFoundException(
                'Pas de produit trouvé avec cet id :' . $id
            );
        }

        //Création d'un formulaire
        $form = $this->createFormBuilder($product) //ajout de l'objet
            ->add('name', TextType::class, array('required' => true))
            ->add('description', TextType::class, array('required' => true))
            ->add('image', TextType::class, array('required' => true))
            ->add('price', IntegerType::class, array('required' => true))

            ->add('productType', EntityType::class, ['class' => ProductType::class, 'choice_label' => "name", 'mapped' => true], array('required' => true))
            ->add('category', EntityType::class, ['class' => TypeCategory::class, 'choice_label' => "name", 'mapped' => true], array('required' => true))

            ->add('save', SubmitType::class, ['label' => "Update le produit"])
            ->getForm();

        //Récupération des données
        $form->handleRequest($request);

        //Teste si le formulaire est submit et valide
        if ($form->isSubmitted() && $form->isValid()) {
            $product->setName($form->get('name')->getData());
            $product->setDescription($form->get('description')->getData());
            $product->setImage($form->get('image')->getData());
            $product->setPrice($form->get('price')->getData());
            $product->setProductType($form->get("productType")->getData());
            $product->setCategory($form->get("category")->getData());

            //Exectution de la requête
            $entityManager->flush();

            //Redirection
            return $this->redirectToRoute('adminProducts');
        }

        return $this->render('administration/product/update.html.twig', [
            'controller_name' => 'AdminController',
            'form' => $form->createView(),
            'action' => 'Update un produit',
            'types' => $types,
            'product' => $product,
        ]);
    }

    // end : UPDATE PRODUCT //

    // UPDATE PRODUCT INFORMATIONS //

    /**
     * @Route("/administration/produits/update/{id}/informations", name="update_product")
     */
    public function updateInfoProduct(int $id, EntityManagerInterface $entityManager, Request $request): Response
    {
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();
        $product_info = $this->getDoctrine()->getRepository(Product::class)->find($id);

        //Récupération du manager de données
        $entityManager = $this->getDoctrine()->getManager();

        //Sélection d'une donnée par son id
        if ($product_info->getProductType()->getName() == "Boissons") {
            $drinkinfo = $entityManager->getRepository(DrinkInfo::class)->find($product_info->getDrinkInfo()->getId());
            if (!$drinkinfo) {
                throw $this->createNotFoundException(
                    'Pas de drinkInfo trouvé avec cet id :' . $id
                );
            }

            $form = $this->createFormBuilder($drinkinfo)
                ->add('liter', IntegerType::class, array('required' => false))
                ->add('alcohol', IntegerType::class, array('required' => false))
            //->add('type', EntityType::class, ['class' => DrinkType::class, 'choice_label' => "name", 'mapped' => false], array('required' => true))

                ->add('save', SubmitType::class, ['label' => "Update les drink infos"])
                ->getForm();

            $form->handleRequest($request);

            //Teste si le formulaire est submit et valide
            if ($form->isSubmitted() && $form->isValid()) {

                $drinkinfo->setLiter($form->get("liter")->getData());
                $drinkinfo->setAlcohol($form->get("alcohol")->getData());
                //$drinkinfo->setType($form->get("type")->getData());

                //Exectution de la requête
                $entityManager->flush();

                //Redirection
                return $this->redirectToRoute('adminUpdateProduct', ['id' => $id]);
            }
        }
        if ($product_info->getProductType()->getName() == "Plats" || $product_info->getProductType()->getName() == "Snacks") {
            $foodinfo = $entityManager->getRepository(FoodInfo::class)->find($product_info->getFoodInfo()->getId());
            if (!$foodinfo) {
                throw $this->createNotFoundException(
                    'Pas de foodInfo trouvé avec cet id :' . $id
                );
            }

            $form = $this->createFormBuilder($foodinfo)
                ->add('fat', IntegerType::class, array('required' => false))
                ->add('energy', IntegerType::class, array('required' => false))
                ->add('glucid', IntegerType::class, array('required' => false))
                ->add('protein', IntegerType::class, array('required' => false))
                ->add('sugar', IntegerType::class, array('required' => false))
                ->add('salt', IntegerType::class, array('required' => false))

                ->add('save', SubmitType::class, ['label' => "Update les food infos"])
                ->getForm();

            $form->handleRequest($request);

            //Teste si le formulaire est submit et valide
            if ($form->isSubmitted() && $form->isValid()) {

                $foodinfo->setFat($form->get("fat")->getData());
                $foodinfo->setEnergy($form->get("energy")->getData());
                $foodinfo->setGlucid($form->get("glucid")->getData());
                $foodinfo->setProtein($form->get("protein")->getData());
                $foodinfo->setSugar($form->get("sugar")->getData());
                $foodinfo->setSalt($form->get("salt")->getData());

                //Exectution de la requête
                $entityManager->flush();

                //Redirection
                return $this->redirectToRoute('adminProducts', ['id' => $id]);
            }
        }

        return $this->render('administration/product/update-informations.html.twig', [
            'controller_name' => 'AdminController',
            'form' => $form->createView(),
            'action' => 'Update les infos produits',
            'types' => $types,
            'product_info' => $product_info,
        ]);
    }

    // end : UPDATE PRODUCT INFORMATIONS //

    // REMOVE PRODUCT //

    /**
     * @Route("/administration/supprimer-produit/{id}", name="remove_product")
     */
    public function removeProduct(int $id): Response
    {
        // Récupération du manager de données
        $entityManager = $this->getDoctrine()->getManager();

        // Sélection d'une donnée par son ID
        $product = $entityManager->getRepository(Product::class)->find($id);

        // Vérification si le produit existe bien
        if (!$product) {
            throw $this->createNotFoundException(
                "Aucun produit ne correspond à cet ID" .$id
            );
        }

        // Supression de l'objet
        $entityManager->remove($product);

        // Execution de la requète
        $entityManager->flush();

        // Redirection
        return $this->redirectToRoute('adminProducts', [
            'id' => $product->getId()
        ]);
    }

    // end : REMOVE PRODUCT //

    // REMOVE PRODUCT INFORMATIONS //

    /**
     * @Route("/administration/produits/update/{id}/informations/remove", name="remove_product")
     */
    public function removeProductInfos(int $id): Response
    {
        //Récupération du manager de données
        $entityManager = $this->getDoctrine()->getManager();
        $products = $this->getDoctrine()->getRepository(Product::class)->find($id);

        if ($products->getProductType()->getName() == "Boissons") {
            $drinkinfo = $entityManager->getRepository(DrinkInfo::class)->find($products->getDrinkInfo()->getId());

            //Vérification si le produit existe bien
            if (!$drinkinfo) {
                throw $this->createNotFoundException(
                    'Pas de drink info trouvé avec cet id :' . $id
                );
            }

            //Suppression de l'objet

            $products->setDrinkInfo($entityManager->remove($drinkinfo));

            //Exectution de la requête
            $entityManager->flush();

            //Redirection
            return $this->redirectToRoute('product', ['id' => $id]);
        }

        if ($products->getProductType()->getName() == "Plats" || $products->getProductType()->getName() == "Snacks") {
            $foodinfo = $entityManager->getRepository(FoodInfo::class)->find($products->getFoodInfo()->getId());

            //Vérification si le produit existe bien
            if (!$foodinfo) {
                throw $this->createNotFoundException(
                    'Pas de drink info trouvé avec cet id :' . $id
                );
            }

            //Suppression de l'objet

            $products->setFoodInfo($entityManager->remove($foodinfo));

            //Exectution de la requête
            $entityManager->flush();

            //Redirection
            return $this->redirectToRoute('adminProducts', ['id' => $id]);
        }
    }

    // end : REMOVE PRODUCT INFORMATIONS //

    // CONTENT //

    /**
     * @Route("/administration/contenu", name="content")
     */
    public function contents(): Response
    {
        $contents = $this->getDoctrine()->getRepository(Content::class)->findAll();

        return $this->render('administration/contents.html.twig', [
            'controller_name' => 'AdministrationController',
            'contents' => $contents
        ]);
    }

    // end : CONTENT //

    // EDIT CONTENT //

    /**
     * @Route("/administration/contenu/{id}/edit", name="edit_content")
     */
    public function editContent(int $id , EntityManagerInterface $entityManager, Request $request): Response
    {
        $types = $this->getDoctrine()->getRepository(ProductType::class)->findAll();

        //Récupération du manager de données
        $entityManager = $this->getDoctrine()->getManager();

        //Sélection d'une donnée par son id
        $content = $entityManager->getRepository(Content::class)->find($id);

        //Vérification si le produit existe bien
        if (!$content) {
            throw $this->createNotFoundException(
                'Pas de contenu trouvé avec cet id :' . $id
            );
        }

        //Création d'un formulaire
        $form = $this->createFormBuilder($content) //ajout de l'objet
            ->add('text', TextType::class, array('required' => true))

            ->add('save', SubmitType::class, ['label' => "Update le contenu"])
            ->getForm();

        //Récupération des données
        $form->handleRequest($request);

        //Teste si le formulaire est submit et valide
        if ($form->isSubmitted() && $form->isValid()) {
            $content->setText($form->get('text')->getData());

            //Exectution de la requête
            $entityManager->flush();

            //Redirection
            return $this->redirectToRoute('index');
        }

        return $this->render('administration/edit-content.html.twig', [
            'controller_name' => 'AdminController',
            'form' => $form->createView(),
            'action' => 'Editer le contenu',
            'types' => $types
        ]);
    }

    // end : EDIT CONTENT //

}

